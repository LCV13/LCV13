


> Soy desarrolladora Web Front-End casi completamente autodidacta desde los 15 años. Mi filosofía en el trabajo consiste en mantener una postura agnóstica frente a las diferentes tecnologías y lenguajes de programación, entendiendolas como herramientas y viendo sus beneficios ante otras opciones dependiendo del problema que se quiera solucionar. <br>
Estudio cosas nuevas y programo todos los días de la semana.

Lenguajes/tecnologías que domino:
* HTML5
* CSS3
* BOOTSTRAP4
* MATERIALIZE
* JQUERY
* ES6
* VUE 
* WordPress
